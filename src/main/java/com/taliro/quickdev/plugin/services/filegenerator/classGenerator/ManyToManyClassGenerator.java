package com.taliro.quickdev.plugin.services.filegenerator.classGenerator;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.taliro.quickdev.plugin.fromxsd.DomainModels;
import com.taliro.quickdev.plugin.services.Fields;
import com.taliro.quickdev.plugin.services.filegenerator.GeneratorCommon;
import com.taliro.quickdev.plugin.services.filegenerator.constant.ClassNameUtils;
import com.taliro.quickdev.plugin.util.Strings;

import javax.lang.model.element.Modifier;

public class ManyToManyClassGenerator implements TypeGenerator {

    @Override
    public boolean isTypeOf(Fields.Field fied) {
        return fied.hasType(DomainModels.Entity.ManyToMany.class);
    }

    @Override
    public void apply(Fields.Field field, TypeSpec.Builder builder, MethodSpec.Builder constructor) {
        FieldSpec.Builder fieldBuilder = FieldSpec
            .builder(field.getType(), field.getName(), Modifier.PRIVATE)
            .initializer("$N", "new java.util.HashSet<>()");

        GeneratorCommon.addAnnotationTitle(fieldBuilder, field.getTitle());

        builder.addMethod(GeneratorCommon.generateAddToList(field.getName(), field.getOriginType(), field.getMappedBy()));
        AnnotationSpec.Builder annoBuilder;
        if (field.getMappedBy() != null) {
            annoBuilder = AnnotationSpec.builder(ClassNameUtils.MANY_TO_MANY).addMember("mappedBy", "$S", field.getMappedBy());
            builder.addMethod(GeneratorCommon.generateSetterToList(field.getName(), field.getType(), field.getMappedBy()));
        } else {
            annoBuilder =
                AnnotationSpec
                    .builder(ClassNameUtils.MANY_TO_MANY)
                    .addMember("fetch", "$L", "jakarta.persistence.FetchType.EAGER")
                    .addMember("cascade", "$L", "{ jakarta.persistence.CascadeType.ALL }");
            Strings currentEntityNameStrings = Strings.of(field.getCurrentEntityName()).convertCamelToSnake();
            Strings targetEntityNameStrings = Strings
                .of(field.getOriginType().toString())
                .split("\\.")
                .rangeFrom(-1)
                .toStrings()
                .convertCamelToSnake()
                .trim();
            builder.addMethod(
                GeneratorCommon.generateRemoveSet(
                    field.getName(),
                    field.getOriginType(),
                    Strings.of(field.getCurrentEntityName()).join("Set").convertToCamelCase().toString()
                )
            );
            fieldBuilder.addAnnotation(
                AnnotationSpec
                    .builder(ClassNameUtils.JOIN_TABLE)
                    .addMember("name", "$S", currentEntityNameStrings.join("_").join(targetEntityNameStrings))
                    .addMember("joinColumns", "@jakarta.persistence.JoinColumn(name = $S)", currentEntityNameStrings.join(".id"))
                    .addMember(
                        "inverseJoinColumns",
                        "@jakarta.persistence.JoinColumn(name = $S)",
                        targetEntityNameStrings.join(".id")
                    )
                    .build()
            );
            builder.addMethod(GeneratorCommon.generateSetterMany(field.getName(), field.getType()));
        }

        fieldBuilder.addAnnotation(annoBuilder.build());
        fieldBuilder.addAnnotation(
            AnnotationSpec
                .builder(ClassNameUtils.JSON_SERIALIZE)
                .addMember("using", "$L", "com.taliro.quickdev.common.service.ChildListSerializer.class")
                .build()
        );

        builder.addMethod(GeneratorCommon.generateGetter(field.getName(), field.getType()));

        builder.addField(fieldBuilder.build());
        constructor.addParameter(field.getType(), field.getName()).addStatement("this.$L = $L", field.getName(), field.getName());
    }
}
