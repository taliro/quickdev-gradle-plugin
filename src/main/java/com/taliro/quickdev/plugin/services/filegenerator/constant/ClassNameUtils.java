package com.taliro.quickdev.plugin.services.filegenerator.constant;

import com.squareup.javapoet.ClassName;

public final class ClassNameUtils {

    public static final ClassName OVERRIDE = ClassName.get("java.lang", "Override");
    public static final ClassName STRING = ClassName.get("java.lang", "String");
    public static final ClassName BYTE = ClassName.get("java.lang", "Byte");
    public static final ClassName BOOLEAN = ClassName.get("java.lang", "Boolean");
    public static final ClassName LOCALDATE = ClassName.get("java.time", "LocalDate");
    public static final ClassName LOCALDATETIME = ClassName.get("java.time", "LocalDateTime");
    public static final ClassName BIGDECIMAL = ClassName.get("java.math", "BigDecimal");
    public static final ClassName INTEGER = ClassName.get("java.lang", "Integer");
    public static final ClassName LONG = ClassName.get("java.lang", "Long");
    public static final ClassName SET = ClassName.get("java.util", "Set");
    public static final ClassName LIST = ClassName.get("java.util", "List");
    public static final ClassName COLUMN = ClassName.get("jakarta.persistence", "Column");
    public static final ClassName TITLE = ClassName.get("com.taliro.quickdev.common.model", "Title");
    public static final ClassName ID = ClassName.get("jakarta.persistence", "Id");
    public static final ClassName GENERATED_VALUE = ClassName.get("jakarta.persistence", "GeneratedValue");
    public static final ClassName SEQUENCE_GENERATOR = ClassName.get("jakarta.persistence", "SequenceGenerator");
    public static final ClassName JOIN_TABLE = ClassName.get("jakarta.persistence", "JoinTable");
    public static final ClassName OPTIONAL = ClassName.get("java.util", "Optional");
    public static final ClassName PANACHE_REPOSITORY = ClassName.get("io.quarkus.hibernate.orm.panache", "PanacheRepository");
    public static final ClassName JPA_REPOSITORY = ClassName.get("com.taliro.quickdev.common.model", "JpaRepository");
    public static final ClassName APPLICATION_SCOPED = ClassName.get("jakarta.enterprise.context", "ApplicationScoped");
    public static final ClassName WIDGET = ClassName.get("com.taliro.quickdev.common.model", "Widget");
    public static final ClassName OBJECT = ClassName.get("java.lang", "Object");
    public static final ClassName ENTITY = ClassName.get("jakarta.persistence", "Entity");
    public static final ClassName FEATURE = ClassName.get("com.taliro.quickdev.common.model", "Feature");
    public static final ClassName ABSTRACT_MODEL = ClassName.get("com.taliro.quickdev.common.model", "AbstractModel");
    public static final ClassName SERIALIZABLE = ClassName.get("java.io", "Serializable");
    public static final ClassName TABLE = ClassName.get("jakarta.persistence", "Table");
    public static final ClassName JSON_SERIALIZE = ClassName.get("com.fasterxml.jackson.databind.annotation", "JsonSerialize");
    public static final ClassName MANY_TO_MANY = ClassName.get("jakarta.persistence", "ManyToMany");
    public static final ClassName MANY_TO_ONE = ClassName.get("jakarta.persistence", "ManyToOne");
    public static final ClassName ONE_TO_MANY = ClassName.get("jakarta.persistence", "OneToMany");
    public static final ClassName JOIN_COLUMN = ClassName.get("jakarta.persistence", "JoinColumn");
}
