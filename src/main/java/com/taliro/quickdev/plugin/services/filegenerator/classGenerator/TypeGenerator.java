package com.taliro.quickdev.plugin.services.filegenerator.classGenerator;

import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.taliro.quickdev.plugin.services.Fields;

public interface TypeGenerator {
    boolean isTypeOf(Fields.Field field);

    void apply(Fields.Field field, TypeSpec.Builder builder, MethodSpec.Builder constructor);
}
