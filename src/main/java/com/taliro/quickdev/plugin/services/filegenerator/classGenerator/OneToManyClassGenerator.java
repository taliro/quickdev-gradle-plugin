package com.taliro.quickdev.plugin.services.filegenerator.classGenerator;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.taliro.quickdev.plugin.fromxsd.DomainModels;
import com.taliro.quickdev.plugin.services.Fields;
import com.taliro.quickdev.plugin.services.filegenerator.GeneratorCommon;
import com.taliro.quickdev.plugin.services.filegenerator.constant.ClassNameUtils;

import javax.lang.model.element.Modifier;

public class OneToManyClassGenerator implements TypeGenerator {

    @Override
    public boolean isTypeOf(Fields.Field field) {
        return field.hasType(DomainModels.Entity.OneToMany.class);
    }

    @Override
    public void apply(Fields.Field field, TypeSpec.Builder builder, MethodSpec.Builder constructor) {
        FieldSpec.Builder fieldBuilder = FieldSpec
            .builder(field.getType(), field.getName(), Modifier.PRIVATE)
            .initializer("$N", "new java.util.ArrayList<>()");

        GeneratorCommon.addAnnotationTitle(fieldBuilder, field.getTitle());
        AnnotationSpec.Builder annoBuilder = AnnotationSpec
            .builder(ClassNameUtils.ONE_TO_MANY)
            .addMember("fetch", "$L", "jakarta.persistence.FetchType.LAZY")
            .addMember(
                "cascade",
                "$L",
                "{ jakarta.persistence.CascadeType.PERSIST, jakarta.persistence.CascadeType.MERGE, jakarta.persistence.CascadeType.REMOVE }"
            );

        annoBuilder.addMember("mappedBy", "$S", field.getMappedBy());
        builder.addMethod(GeneratorCommon.generateAddToChild(field.getName(), field.getOriginType(), field.getMappedBy()));

        fieldBuilder.addAnnotation(annoBuilder.build());
        fieldBuilder.addAnnotation(
            AnnotationSpec
                .builder(ClassNameUtils.JSON_SERIALIZE)
                .addMember("using", "$L", "com.taliro.quickdev.common.service.ChildListSerializer.class")
                .build()
        );

        builder
            .addMethod(GeneratorCommon.generateGetter(field.getName(), field.getType()))
            .addMethod(GeneratorCommon.generateSetterToChild(field.getName(), field.getType(), field.getMappedBy()));

        builder.addField(fieldBuilder.build());
        constructor.addParameter(field.getType(), field.getName()).addStatement("this.$L = $L", field.getName(), field.getName());
    }
}
