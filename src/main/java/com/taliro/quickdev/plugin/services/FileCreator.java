package com.taliro.quickdev.plugin.services;

import com.squareup.javapoet.JavaFile;
import com.taliro.quickdev.plugin.services.filegenerator.SubProject;

import java.io.IOException;

public class FileCreator {

    public static final String pathToDropFile = "build/generated/quickdev/src/main/java";
    private final JavaFile javaFile;
    private final SubProject subProject;

    public FileCreator(JavaFile javaFile, SubProject subProject) {
        this.javaFile = javaFile;
        this.subProject = subProject;
    }

    public void generateFile() throws IOException {
        javaFile.writeTo(subProject.getPath().resolve(pathToDropFile));
    }
}
