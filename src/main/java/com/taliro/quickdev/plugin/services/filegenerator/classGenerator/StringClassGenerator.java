package com.taliro.quickdev.plugin.services.filegenerator.classGenerator;

import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.taliro.quickdev.plugin.fromxsd.DomainModels;
import com.taliro.quickdev.plugin.services.Fields;
import com.taliro.quickdev.plugin.services.filegenerator.GeneratorCommon;

import javax.lang.model.element.Modifier;
import java.util.ArrayList;
import java.util.List;

public class StringClassGenerator implements TypeGenerator {

    @Override
    public boolean isTypeOf(Fields.Field field) {
        return field.hasType(DomainModels.Entity.String.class);
    }

    @Override
    public void apply(Fields.Field field, TypeSpec.Builder builder, MethodSpec.Builder constructor) {
        FieldSpec.Builder fieldBuilder = FieldSpec.builder(field.getType(), field.getName(), Modifier.PRIVATE);
        GeneratorCommon.addAnnotationColumn(fieldBuilder, field);
        GeneratorCommon.addAnnotationTitle(fieldBuilder, field.getTitle());
        MethodSpec methodGetter = GeneratorCommon.generateGetter(field.getName(), field.getType());
        MethodSpec methodSetter = GeneratorCommon.generateSetter(field.getName(), field.getType());

        List<String> widgetValues = new ArrayList<>();
        getPassword(field, widgetValues);
        GeneratorCommon.addWidgets(fieldBuilder, widgetValues);

        builder.addField(fieldBuilder.build());
        builder.addMethod(methodGetter).addMethod(methodSetter);
        constructor.addParameter(field.getType(), field.getName()).addStatement("this.$L = $L", field.getName(), field.getName());

        if (field.isPrincipalField()) {
            builder.addMethod(GeneratorCommon.addMethodPrincipalField(field.getName(), false));
        }
    }

    private void getPassword(Fields.Field field, List<String> widgetValues) {
        if (field.isPassword()) {
            widgetValues.add("password");
        }
    }
}
