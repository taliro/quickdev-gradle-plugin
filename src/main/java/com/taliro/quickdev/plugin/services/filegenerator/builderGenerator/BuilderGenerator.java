package com.taliro.quickdev.plugin.services.filegenerator.builderGenerator;

import com.squareup.javapoet.TypeSpec;
import com.taliro.quickdev.plugin.services.Fields;

public interface BuilderGenerator {
    boolean isTypeOf(Fields.Field field);

    void apply(Fields.Field field, TypeSpec.Builder builder);
}
