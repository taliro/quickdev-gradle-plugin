package com.taliro.quickdev.plugin.services.filegenerator;

import com.squareup.javapoet.*;
import com.taliro.quickdev.plugin.services.Fields;
import com.taliro.quickdev.plugin.services.filegenerator.constant.ClassNameUtils;
import com.taliro.quickdev.plugin.util.Strings;

import javax.lang.model.element.Modifier;
import java.util.List;
import java.util.stream.Collectors;

public final class GeneratorCommon {

    public static MethodSpec generateSetterBuilder(String fieldName, TypeName fieldType) {
        return MethodSpec
            .methodBuilder(fieldName)
            .addModifiers(Modifier.PUBLIC)
            .returns(ClassName.bestGuess("Builder"))
            .addParameter(fieldType, fieldName)
            .addStatement("this.$N = $N", fieldName, fieldName)
            .addStatement("return this")
            .build();
    }

    public static MethodSpec generateGetter(String fieldName, TypeName fieldType) {
        return MethodSpec
            .methodBuilder("get" + capitalize(fieldName))
            .addModifiers(Modifier.PUBLIC)
            .returns(fieldType)
            .addStatement("return $N", fieldName)
            .build();
    }

    public static MethodSpec generateGetterBoolean(String fieldName, TypeName fieldType) {
        return MethodSpec
            .methodBuilder("is" + capitalize(fieldName))
            .addModifiers(Modifier.PUBLIC)
            .returns(fieldType)
            .addStatement("return $N", fieldName)
            .build();
    }

    public static MethodSpec generateAddToList(String fieldName, TypeName fieldType, String mappedBy) {
        MethodSpec.Builder child = MethodSpec
            .methodBuilder("add" + capitalize(fieldName.replaceAll("Set|List", "")))
            .addModifiers(Modifier.PUBLIC)
            .returns(TypeName.VOID)
            .addParameter(fieldType, "child")
            .addStatement("this.$N.add(child)", fieldName);

        if (mappedBy != null) {
            child.addStatement("child.add$N(this)", capitalize(mappedBy.replaceAll("Set|List", "")));
        }

        return child.build();
    }

    public static MethodSpec generateAddToChild(String fieldName, TypeName fieldType, String mappedBy) {
        return MethodSpec
            .methodBuilder("add" + capitalize(fieldName))
            .addModifiers(Modifier.PUBLIC)
            .returns(TypeName.VOID)
            .addParameter(fieldType, "child")
            .addStatement("child.set$N(this)", capitalize(mappedBy))
            .addStatement("this.$N.add(child)", fieldName)
            .build();
    }

    public static MethodSpec generateRemoveSet(String fieldName, TypeName fieldType, String targetFieldName) {
        return MethodSpec
            .methodBuilder("remove" + capitalize(fieldName))
            .addModifiers(Modifier.PUBLIC)
            .addParameter(fieldType, fieldName)
            .addStatement("this.$N.remove($N)", fieldName, fieldName)
            .addStatement("$N.get$N().remove(this)", fieldName, capitalize(targetFieldName))
            .build();
    }

    public static MethodSpec generateSetter(String fieldName, TypeName fieldType) {
        return MethodSpec
            .methodBuilder("set" + capitalize(fieldName))
            .addModifiers(Modifier.PUBLIC)
            .addParameter(fieldType, fieldName)
            .addStatement("this.$N = $N", fieldName, fieldName)
            .build();
    }

    public static MethodSpec generateSetterMany(String fieldName, TypeName fieldType) {
        return MethodSpec
            .methodBuilder("set" + capitalize(fieldName))
            .addModifiers(Modifier.PUBLIC)
            .addParameter(fieldType, fieldName)
            .addStatement("this.$N.clear()", fieldName)
            .addStatement("this.$N.addAll($N)", fieldName, fieldName)
            .build();
    }

    public static void addWidgets(FieldSpec.Builder builder, List<String> widgetValues) {
        if (widgetValues.isEmpty()) {
            return;
        }

        checkContainPasswordWidget(builder, widgetValues);

        builder.addAnnotation(
            AnnotationSpec
                .builder(ClassNameUtils.WIDGET)
                .addMember("value", "{$L}", widgetValues.stream().map("\"%s\""::formatted).collect(Collectors.joining(", ")))
                .build()
        );
    }

    private static void checkContainPasswordWidget(FieldSpec.Builder builder, List<String> widgetValues) {
        if (widgetValues.contains("checkContainPasswordWidget")) {
            builder.addAnnotation(
                AnnotationSpec
                    .builder(ClassNameUtils.JSON_SERIALIZE)
                    .addMember("using", "$L", "com.taliro.quickdev.common.service.PasswordSerializer")
                    .build()
            );
        }
    }

    public static MethodSpec generateSetterToChild(String fieldName, TypeName fieldType, String mappedBy) {
        return MethodSpec
            .methodBuilder("set" + capitalize(fieldName))
            .addModifiers(Modifier.PUBLIC)
            .addParameter(fieldType, fieldName)
            .addStatement("$N.forEach(it -> it.set$N(this))", fieldName, capitalize(mappedBy))
            .addStatement("this.$N = $N", fieldName, fieldName)
            .build();
    }

    public static MethodSpec generateSetterToList(String fieldName, TypeName fieldType, String mappedBy) {
        return MethodSpec
            .methodBuilder("set" + capitalize(fieldName))
            .addModifiers(Modifier.PUBLIC)
            .addParameter(fieldType, fieldName)
            .addStatement("$N.forEach(it -> it.add$N(this))", fieldName, capitalize(mappedBy.replaceAll("Set|List", "")))
            .addStatement("this.$N = $N", fieldName, fieldName)
            .build();
    }

    public static MethodSpec addMethodPrincipalField(String fieldName, boolean needCastToString) {
        String statement = "return java.util.Optional.ofNullable($N).orElse(null)";
        if (needCastToString) {
            statement = "return java.util.Optional.ofNullable($N).map(Object::toString).orElse(null)";
        }

        return MethodSpec
            .methodBuilder("getPrincipalField")
            .addModifiers(Modifier.PUBLIC)
            .addAnnotation(ClassName.bestGuess("java.lang.Override"))
            .returns(ClassName.bestGuess(String.class.getName()))
            .addStatement(statement, fieldName)
            .build();
    }

    public static String capitalize(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    public static void addAnnotationColumn(FieldSpec.Builder builder, Fields.Field field) {
        AnnotationSpec.Builder column = AnnotationSpec.builder(ClassNameUtils.COLUMN);

        if (field.isUnique()) {
            column.addMember("unique", "$L", "true");
        }

        if (field.getNameColumn() != null) {
            column.addMember("name", "$S", field.getNameColumn());
        }

        if (!field.isNullable()) {
            column.addMember("nullable", "$L", false);
        }

        builder.addAnnotation(column.build());
    }

    public static void addAnnotationTitle(FieldSpec.Builder builder, String title) {
        builder.addAnnotation(AnnotationSpec.builder(ClassNameUtils.TITLE).addMember("value", "$S", title).build());
    }

    public static void addIdentityField(TypeSpec.Builder builder, String tablename) {
        tablename = Strings.of(tablename).replaceAll("\"", "").convertCamelToSnake().toString();
        builder.addField(
            FieldSpec
                .builder(ClassNameUtils.LONG, "id", Modifier.PRIVATE)
                .addAnnotation(ClassNameUtils.ID)
                .addAnnotation(
                    AnnotationSpec
                        .builder(ClassNameUtils.GENERATED_VALUE)
                        .addMember("strategy", "$N", "jakarta.persistence.GenerationType.SEQUENCE")
                        .addMember("generator", "$S", "%s_sequence".formatted(tablename))
                        .build()
                )
                .addAnnotation(
                    AnnotationSpec
                        .builder(ClassNameUtils.SEQUENCE_GENERATOR)
                        .addMember("name", "$S", "%s_sequence".formatted(tablename))
                        .addMember("sequenceName", "$S", "%s_seq".formatted(tablename))
                        .addMember("allocationSize", "$L", "1")
                        .build()
                )
                .addAnnotation(AnnotationSpec.builder(ClassNameUtils.TITLE).addMember("value", "$S", "Identifiant").build())
                .build()
        );
        builder.addMethod(GeneratorCommon.generateGetter("id", ClassNameUtils.LONG));
    }
}
