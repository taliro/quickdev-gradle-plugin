package com.taliro.quickdev.plugin.services.filegenerator;

import com.squareup.javapoet.*;
import com.taliro.quickdev.plugin.fromxsd.DomainModels;
import com.taliro.quickdev.plugin.services.Fields;
import com.taliro.quickdev.plugin.services.FileCreator;
import com.taliro.quickdev.plugin.services.filegenerator.constant.ClassNameUtils;

import javax.lang.model.element.Modifier;
import java.io.IOException;

public class RepositoryFileGenerator implements FileGenerator {

    private final Fields fields;
    private final DomainModels domainModels;
    private final SubProject subProject;

    public RepositoryFileGenerator(DomainModels domainModels, Fields fields, SubProject subProject) {
        this.fields = fields;
        this.domainModels = domainModels;
        this.subProject = subProject;
    }

    @Override
    public void generateFile() throws IOException {
        TypeName typeName = ParameterizedTypeName.get(
            ClassNameUtils.JPA_REPOSITORY,
            ClassName.get(domainModels.getEntity().getPackage(), domainModels.getEntity().getName())
        );

        TypeSpec.Builder builder = TypeSpec
            .classBuilder(domainModels.getEntity().getName() + "Repository")
            .addSuperinterface(typeName)
            .addModifiers(Modifier.PUBLIC);

        fields.computedQueryFields(field ->
            builder.addMethod(
                new QueryBuilder(field.getName(), fields)
                    .toMethods(domainModels.getEntity().getPackage(), domainModels.getEntity().getName())
            )
        );

        builder.addAnnotation(ClassNameUtils.APPLICATION_SCOPED);
        new FileCreator(
            JavaFile.builder(domainModels.getEntity().getPackage() + ".repository", builder.build()).build(),
            subProject
        )
            .generateFile();
    }
}
