package com.taliro.quickdev.plugin.services.filegenerator;

import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.taliro.quickdev.plugin.fromxsd.DomainModels;
import com.taliro.quickdev.plugin.services.FileCreator;
import com.taliro.quickdev.plugin.services.filegenerator.constant.ClassNameUtils;

import javax.lang.model.element.Modifier;
import java.io.IOException;

public class EnumFileGenerator implements FileGenerator {

    private final DomainModels domainModels;
    private final SubProject subProject;

    public EnumFileGenerator(DomainModels domainModels, SubProject subProject) {
        this.domainModels = domainModels;
        this.subProject = subProject;
    }

    @Override
    public void generateFile() throws IOException {
        for (DomainModels.Enum anEnum : domainModels.getEnum()) {
            TypeSpec.Builder builder = TypeSpec.enumBuilder(anEnum.getName()).addModifiers(Modifier.PUBLIC);

            for (DomainModels.Enum.Item item : anEnum.getItem()) {
                builder.addEnumConstant(item.getName(), TypeSpec.anonymousClassBuilder("$S", item.getValue()).build());
            }

            /* CONSTRUCTOR */
            builder.addMethod(
                MethodSpec
                    .constructorBuilder()
                    .addParameter(ClassNameUtils.STRING, "value")
                    .addStatement("this.value = value")
                    .build()
            );

            /* FIELD */
            builder.addField(FieldSpec.builder(ClassNameUtils.STRING, "value", Modifier.PRIVATE, Modifier.FINAL).build());

            /* METHOD */
            builder.addMethod(
                MethodSpec.methodBuilder("getValue").returns(ClassNameUtils.STRING).addStatement("return this.value").build()
            );

            new FileCreator(JavaFile.builder(anEnum.getPackage(), builder.build()).build(), subProject).generateFile();
        }
    }
}
