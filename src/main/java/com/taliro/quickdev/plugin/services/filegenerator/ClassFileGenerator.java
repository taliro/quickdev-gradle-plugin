package com.taliro.quickdev.plugin.services.filegenerator;

import com.squareup.javapoet.*;
import com.taliro.quickdev.plugin.fromxsd.DomainModels;
import com.taliro.quickdev.plugin.services.Fields;
import com.taliro.quickdev.plugin.services.FileCreator;
import com.taliro.quickdev.plugin.services.filegenerator.builderGenerator.*;
import com.taliro.quickdev.plugin.services.filegenerator.classGenerator.*;
import com.taliro.quickdev.plugin.services.filegenerator.constant.ClassNameUtils;
import org.jetbrains.annotations.NotNull;

import javax.lang.model.element.Modifier;
import java.io.IOException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;

public class ClassFileGenerator implements FileGenerator {

    private final Fields fields;
    private final String tableName;
    private final SubProject subProject;
    private final String entityName;
    private final String packageName;

    private final Set<TypeGenerator> typeGenerators = new HashSet<>();

    private final Set<BuilderGenerator> builderGenerators = new HashSet<>();
    private final String feature;

    public ClassFileGenerator(DomainModels domainModels, Fields fields, SubProject subProject) {
        this.fields = fields;
        this.subProject = subProject;
        this.entityName = domainModels.getEntity().getName();
        this.tableName = domainModels.getEntity().getTable();
        this.packageName = domainModels.getEntity().getPackage();
        this.feature = domainModels.getEntity().getFeature();

        typeGenerators.add(new BinaryClassGenerator());
        typeGenerators.add(new BooleanClassGenerator());
        typeGenerators.add(new DateClassGenerator());
        typeGenerators.add(new DateTimeClassGenerator());
        typeGenerators.add(new DecimalClassGenerator());
        typeGenerators.add(new EnumerableClassGenerator());
        typeGenerators.add(new IntegerClassGenerator());
        typeGenerators.add(new LongClassGenerator());
        typeGenerators.add(new ManyToManyClassGenerator());
        typeGenerators.add(new ManyToOneClassGenerator());
        typeGenerators.add(new OneToManyClassGenerator());
        typeGenerators.add(new StringClassGenerator());

        builderGenerators.add(new BinaryBuilderGenerator());
        builderGenerators.add(new BooleanBuilderGenerator());
        builderGenerators.add(new DateBuilderGenerator());
        builderGenerators.add(new DateTimeBuilderGenerator());
        builderGenerators.add(new DecimalBuilderGenerator());
        builderGenerators.add(new EnumerableBuilderGenerator());
        builderGenerators.add(new IntegerBuilderGenerator());
        builderGenerators.add(new LongBuilderGenerator());
        builderGenerators.add(new ManyToManyBuilderGenerator());
        builderGenerators.add(new ManyToOneBuilderGenerator());
        builderGenerators.add(new OneToManyBuilderGenerator());
        builderGenerators.add(new StringBuilderGenerator());
    }

    public void generateFile() throws IOException {
        var classGenerator = createClass(this.feature);
        addAnnotationTableIfTableIsSpecified(classGenerator);
        TypeSpec.Builder classInnerBuilder = createClassInnerBuilder();
        addMethodBuilder(classGenerator);

        /* CONSTRUCTOR */
        MethodSpec.Builder constructorFull = MethodSpec.constructorBuilder().addModifiers(Modifier.PUBLIC);
        MethodSpec constructorEmpty = MethodSpec.constructorBuilder().addModifiers(Modifier.PUBLIC).build();

        AtomicBoolean columnExist = new AtomicBoolean(false);
        StringBuilder argumentsComputed = new StringBuilder();
        StringBuilder argumentsComputedFull = new StringBuilder();
        fields.computedPrimitiveField(field -> {
            /* CLASS */
            computedClassInstandOf(field, classGenerator, constructorFull);
            /* BUILDER */
            computedBuilderInstandOf(field, classInnerBuilder);

            if (field.isPrincipalField()) {
                columnExist.set(true);
            }

            if (!field.isRelation()) {
                constructArguments(argumentsComputed, field.getName());
            }
            constructArguments(argumentsComputedFull, field.getName());
        });

        addMethodEquals(classGenerator);
        addMethodHashCode(classGenerator, argumentsComputed.toString());
        addColumnNameIfNotExist(columnExist, classGenerator);
        addMethodBuildToBuilder(classInnerBuilder, argumentsComputedFull.toString());
        GeneratorCommon.addIdentityField(classGenerator, Optional.ofNullable(tableName).orElse(entityName));

        /* Builder */
        classGenerator.addType(classInnerBuilder.build()).addMethod(constructorEmpty).addMethod(constructorFull.build());
        new FileCreator(JavaFile.builder(packageName, classGenerator.build()).build(), subProject).generateFile();
    }

    private void addMethodBuildToBuilder(TypeSpec.Builder classInnerBuilder, String arguments) {
        classInnerBuilder.addMethod(
            MethodSpec
                .methodBuilder("build")
                .addModifiers(Modifier.PUBLIC)
                .returns(ClassName.bestGuess(entityName))
                .addStatement("return new $N($L)", entityName, arguments)
                .build()
        );
    }

    private void addColumnNameIfNotExist(AtomicBoolean columnExist, TypeSpec.Builder classGenerator) {
        if (!columnExist.get()) {
            if (fields.getFields().stream().anyMatch(it -> it.getName().equals("name"))) {
                classGenerator.addMethod(GeneratorCommon.addMethodPrincipalField("name", false));
            } else {
                classGenerator.addMethod(GeneratorCommon.addMethodPrincipalField("id", true));
            }
        }
    }

    private static void addMethodBuilder(TypeSpec.Builder classGenerator) {
        classGenerator.addMethod(
            MethodSpec
                .methodBuilder("builder")
                .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
                .returns(ClassName.bestGuess("Builder"))
                .addStatement("return new Builder()")
                .build()
        );
    }

    private void addAnnotationTableIfTableIsSpecified(TypeSpec.Builder classGenerator) {
        if (this.tableName != null) {
            classGenerator.addAnnotation(
                AnnotationSpec.builder(ClassNameUtils.TABLE).addMember("name", "$S", this.tableName).build()
            );
        }
    }

    private static TypeSpec.Builder createClassInnerBuilder() {
        return TypeSpec.classBuilder("Builder").addModifiers(Modifier.PUBLIC, Modifier.STATIC);
    }

    @NotNull
    private TypeSpec.Builder createClass(String feature) {
        TypeSpec.Builder builder = TypeSpec
            .classBuilder(entityName)
            .addAnnotation(AnnotationSpec.builder(ClassNameUtils.ENTITY).build())
            .addModifiers(Modifier.PUBLIC, Modifier.FINAL)
            .superclass(ClassNameUtils.ABSTRACT_MODEL)
            .addSuperinterface(ClassNameUtils.SERIALIZABLE);

        if (feature != null) {
            builder.addAnnotation(AnnotationSpec.builder(ClassNameUtils.FEATURE).addMember("value", "$S", feature).build());
        }

        return builder;
    }

    private static void addMethodHashCode(TypeSpec.Builder classGenerator, String arguments) {
        classGenerator.addMethod(
            MethodSpec
                .methodBuilder("hashCode")
                .returns(ClassName.INT)
                .addModifiers(Modifier.PUBLIC)
                .addStatement("return java.util.Objects.hash($L)", arguments)
                .addAnnotation(ClassNameUtils.OVERRIDE)
                .build()
        );
    }

    private void addMethodEquals(TypeSpec.Builder classGenerator) {
        classGenerator.addMethod(
            MethodSpec
                .methodBuilder("equals")
                .addParameter(ClassNameUtils.OBJECT, "o")
                .returns(ClassName.BOOLEAN)
                .addModifiers(Modifier.PUBLIC)
                .addStatement("if (this == o) return true")
                .addStatement("if (o == null || getClass() != o.getClass()) return false")
                .addStatement("$L classComparator = ($L) o", entityName, entityName)
                .addStatement("return java.util.Objects.equals(id, classComparator.id)")
                .addAnnotation(ClassNameUtils.OVERRIDE)
                .build()
        );
    }

    private static void constructArguments(StringBuilder arguments, String fieldToAdd) {
        if (arguments.isEmpty()) {
            arguments.append("this.").append(fieldToAdd);
        } else {
            arguments.append(",").append("this.").append(fieldToAdd);
        }
    }

    private void computedClassInstandOf(Fields.Field field, TypeSpec.Builder builder, MethodSpec.Builder constructor) {
        TypeGenerator typeGenerator = typeGenerators.stream().filter(it -> it.isTypeOf(field)).findFirst().orElse(null);
        if (typeGenerator == null) return;
        typeGenerator.apply(field, builder, constructor);
    }

    private void computedBuilderInstandOf(Fields.Field field, TypeSpec.Builder builder) {
        BuilderGenerator builderGenerator = builderGenerators.stream().filter(it -> it.isTypeOf(field)).findFirst().orElse(null);
        if (builderGenerator == null) return;
        builderGenerator.apply(field, builder);
    }
}
