package com.taliro.quickdev.plugin.services.filegenerator.builderGenerator;

import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.taliro.quickdev.plugin.fromxsd.DomainModels;
import com.taliro.quickdev.plugin.services.Fields;
import com.taliro.quickdev.plugin.services.filegenerator.GeneratorCommon;

import javax.lang.model.element.Modifier;

public class StringBuilderGenerator implements BuilderGenerator {

    @Override
    public boolean isTypeOf(Fields.Field field) {
        return field.hasType(DomainModels.Entity.String.class);
    }

    @Override
    public void apply(Fields.Field field, TypeSpec.Builder builder) {
        FieldSpec.Builder fieldBuilder = FieldSpec.builder(field.getType(), field.getName(), Modifier.PRIVATE);
        MethodSpec methodGetter = GeneratorCommon.generateSetterBuilder(field.getName(), field.getType());
        builder.addField(fieldBuilder.build());
        builder.addMethod(methodGetter);
    }
}
