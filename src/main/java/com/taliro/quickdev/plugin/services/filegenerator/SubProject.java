package com.taliro.quickdev.plugin.services.filegenerator;

import java.io.File;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SubProject {

    final List<File> domain;
    final Path path;

    public SubProject(File file) {
        this.path = file.toPath();
        this.domain =
            Optional
                .of(path.resolve("src/main/resources/domains"))
                .map(Path::toFile)
                .map(it -> it.listFiles(pathname -> pathname.getName().endsWith(".xml")))
                .map(Arrays::asList)
                .orElse(List.of());
    }

    public List<File> getDomain() {
        return domain;
    }

    public Path getPath() {
        return path;
    }
}
