package com.taliro.quickdev.plugin.services.filegenerator;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.taliro.quickdev.plugin.services.Fields;
import com.taliro.quickdev.plugin.services.filegenerator.constant.ClassNameUtils;

import javax.lang.model.element.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class QueryBuilder {

    boolean hasOne = false;
    final String query;
    final List<String> parameters = new ArrayList<>();
    final StringBuilder queryFinal = new StringBuilder("(");
    final MethodSpec.Builder methodsBuilder;

    public QueryBuilder(String query, Fields fields) {
        this.query = query;

        if (query.startsWith("findOneBy")) {
            hasOne = true;
        }

        this.methodsBuilder = MethodSpec.methodBuilder(query);

        String queryComputed = query.replaceAll("findOneBy|findBy", "");
        List<String> decomposeCamelCase = decomposeMethodName(queryComputed);

        for (String fieldName : decomposeCamelCase) {
            if (!fieldName.equalsIgnoreCase("or") && !fieldName.equalsIgnoreCase("and")) {
                try {
                    Fields.Field field = fields.getByName(fieldName);
                    methodsBuilder.addParameter(ParameterSpec.builder(field.getType(), field.getName(), Modifier.FINAL).build());
                    queryFinal.append(field.getName()).append(" = :").append(field.getName());
                    parameters.add(field.getName());
                } catch (Exception e) {
                    throw new RuntimeException("Field %s not found for query %s".formatted(fieldName, query), e);
                }
            }

            if (fieldName.equalsIgnoreCase("or")) {
                queryFinal.append(") or (");
            }

            if (fieldName.equalsIgnoreCase("and")) {
                queryFinal.append(" and ");
            }
        }

        queryFinal.append(")");
    }

    public MethodSpec toMethods(String packageName, String entityName) {
        ParameterizedTypeName typeName;
        if (hasOne) {
            typeName = ParameterizedTypeName.get(ClassNameUtils.OPTIONAL, ClassName.get(packageName, entityName));

            return methodsBuilder
                .returns(typeName)
                .addModifiers(Modifier.PUBLIC)
                .addStatement("return find($S, $N).firstResultOptional()", queryFinal.toString(), String.join(",", parameters))
                .build();
        } else {
            typeName = ParameterizedTypeName.get(ClassNameUtils.LIST, ClassName.get(packageName, entityName));
            return methodsBuilder
                .returns(typeName)
                .addModifiers(Modifier.PUBLIC)
                .addStatement("return find($S, $N).list()", queryFinal.toString(), String.join(",", parameters))
                .build();
        }
    }

    public static List<String> decomposeMethodName(String query) {
        String regex = "(?i)(and|or)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(query);

        List<String> parts = new ArrayList<>();
        int start = 0;
        while (matcher.find()) {
            int end = matcher.start();
            parts.add(query.substring(start, end));
            parts.add(matcher.group());
            start = matcher.end();
        }
        parts.add(query.substring(start));

        return parts;
    }
}
