package com.taliro.quickdev.plugin.services.filegenerator.classGenerator;

import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.TypeSpec;
import com.taliro.quickdev.plugin.fromxsd.DomainModels;
import com.taliro.quickdev.plugin.services.Fields;
import com.taliro.quickdev.plugin.services.filegenerator.GeneratorCommon;
import com.taliro.quickdev.plugin.services.filegenerator.constant.ClassNameUtils;

import javax.lang.model.element.Modifier;

public class ManyToOneClassGenerator implements TypeGenerator {

    @Override
    public boolean isTypeOf(Fields.Field field) {
        return field.hasType(DomainModels.Entity.ManyToOne.class);
    }

    @Override
    public void apply(Fields.Field field, TypeSpec.Builder builder, MethodSpec.Builder constructor) {
        FieldSpec.Builder fieldBuilder = FieldSpec.builder(field.getType(), field.getName(), Modifier.PRIVATE);

        GeneratorCommon.addAnnotationTitle(fieldBuilder, field.getTitle());
        AnnotationSpec.Builder annoBuilder = AnnotationSpec
            .builder(ClassNameUtils.MANY_TO_ONE)
            .addMember("fetch", "$L", "jakarta.persistence.FetchType.LAZY");

        fieldBuilder.addAnnotation(annoBuilder.build());
        fieldBuilder.addAnnotation(
            AnnotationSpec
                .builder(ClassNameUtils.JSON_SERIALIZE)
                .addMember("using", "$L", "com.taliro.quickdev.common.service.ChildSerializer.class")
                .build()
        );

        if (!field.isNullable() || field.getNameColumn() != null) {
            AnnotationSpec.Builder annoJoinColumn = AnnotationSpec.builder(ClassNameUtils.JOIN_COLUMN);

            if (!field.isNullable()) {
                annoJoinColumn.addMember("nullable", "$L", false);
            }

            if (field.getNameColumn() != null) {
                annoJoinColumn.addMember("name", "$S", field.getNameColumn());
            }

            fieldBuilder.addAnnotation(annoJoinColumn.build());
        }

        builder
            .addMethod(GeneratorCommon.generateGetter(field.getName(), field.getType()))
            .addMethod(GeneratorCommon.generateSetter(field.getName(), field.getType()));

        builder.addField(fieldBuilder.build());
        constructor.addParameter(field.getType(), field.getName()).addStatement("this.$L = $L", field.getName(), field.getName());
    }
}
