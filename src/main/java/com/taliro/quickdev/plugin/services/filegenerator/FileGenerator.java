package com.taliro.quickdev.plugin.services.filegenerator;

import java.io.IOException;

public interface FileGenerator {
    void generateFile() throws IOException;
}
