package com.taliro.quickdev.plugin.services;

import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.taliro.quickdev.plugin.exception.GeneratorException;
import com.taliro.quickdev.plugin.fromxsd.DomainModels;
import com.taliro.quickdev.plugin.services.filegenerator.constant.ClassNameUtils;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public final class Fields {

    private final List<Field> fields;

    public Fields(List<Object> fields, String currentEntityName) {
        this.fields = fields.stream().map(it -> Field.of(it, currentEntityName)).toList();
    }

    public List<Field> getFields() {
        return fields;
    }

    public void computedQueryFields(Consumer<Field> consumer) {
        fields.stream().filter(Field::isQueryField).forEach(consumer);
    }

    public void computedPrimitiveField(Consumer<Field> consumer) {
        fields.stream().filter(Predicate.not(Field::isQueryField)).forEach(consumer);
    }

    public Field getByName(String name) {
        return this.fields.stream().filter(f -> f.name.equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public static final class Field {

        private final String name;
        private final String currentEntityName;
        private String title;
        private String mappedBy;
        private boolean hasRelation = false;
        private TypeName type;
        private TypeName originType;
        private boolean hasPrincipalField = false;
        private boolean hasQuery = false;
        private boolean hasPassword = false;
        private boolean hasUnique = false;
        private boolean hasNullable = false;
        private String nameColumn;
        private final Class<?> originClass;

        public static Field of(Object field, String currentEntityName) {
            try {
                return new Field(field, currentEntityName);
            } catch (Exception e) {
                return null;
            }
        }

        private Field(Object field, String currentEntityName) {
            this.originClass = field.getClass();
            this.currentEntityName = currentEntityName;
            if (field instanceof DomainModels.Entity.Binary f) {
                this.name = f.getName();
                this.title = f.getTitle();
                this.type = ClassNameUtils.BYTE;
                this.nameColumn = f.getNamecolumn();
                this.hasNullable = f.getNullable().equals("true");
            } else if (field instanceof DomainModels.Entity.Enum f) {
                this.name = f.getName();
                this.title = f.getTitle();
                this.type = ClassName.bestGuess(f.getRef());
                this.hasNullable = f.getNullable().equals("true");
            } else if (field instanceof DomainModels.Entity.Boolean f) {
                this.name = f.getName();
                this.title = f.getTitle();
                this.type = ClassNameUtils.BOOLEAN;
                this.nameColumn = f.getNamecolumn();
                this.hasNullable = f.getNullable().equals("true");
            } else if (field instanceof DomainModels.Entity.Date f) {
                this.name = f.getName();
                this.title = f.getTitle();
                this.type = ClassNameUtils.LOCALDATE;
                this.nameColumn = f.getNamecolumn();
                this.hasNullable = f.getNullable().equals("true");
            } else if (field instanceof DomainModels.Entity.Decimal f) {
                this.name = f.getName();
                this.title = f.getTitle();
                this.type = ClassNameUtils.BIGDECIMAL;
                this.nameColumn = f.getNamecolumn();
                this.hasNullable = f.getNullable().equals("true");
            } else if (field instanceof DomainModels.Entity.Datetime f) {
                this.name = f.getName();
                this.title = f.getTitle();
                this.type = ClassNameUtils.LOCALDATETIME;
                this.nameColumn = f.getNamecolumn();
                this.hasNullable = f.getNullable().equals("true");
            } else if (field instanceof DomainModels.Entity.Integer f) {
                this.name = f.getName();
                this.title = f.getTitle();
                this.type = ClassNameUtils.INTEGER;
                this.nameColumn = f.getNamecolumn();
                this.hasNullable = f.getNullable().equals("true");
            } else if (field instanceof DomainModels.Entity.Long f) {
                this.name = f.getName();
                this.title = f.getTitle();
                this.type = ClassNameUtils.LONG;
                this.nameColumn = f.getNamecolumn();
                this.hasNullable = f.getNullable().equals("true");
            } else if (field instanceof DomainModels.Entity.ManyToMany f) {
                this.name = f.getName();
                this.title = f.getTitle();
                this.originType = ClassName.bestGuess(f.getRef());
                this.type = ParameterizedTypeName.get(ClassNameUtils.SET, originType);
                this.mappedBy = f.getMappedBy();
                this.hasRelation = true;
            } else if (field instanceof DomainModels.Entity.ManyToOne f) {
                this.name = f.getName();
                this.title = f.getTitle();
                this.type = ClassName.bestGuess(f.getRef());
                this.hasRelation = true;
                this.nameColumn = f.getNamecolumn();
                this.hasNullable = f.getNullable().equals("true");
            } else if (field instanceof DomainModels.Entity.OneToMany f) {
                this.name = f.getName();
                this.title = f.getTitle();
                this.originType = ClassName.bestGuess(f.getRef());
                this.type = ParameterizedTypeName.get(ClassNameUtils.LIST, originType);
                this.mappedBy = f.getMappedBy();
                this.hasRelation = true;
            } else if (field instanceof DomainModels.Entity.String f) {
                this.name = f.getName();
                this.title = f.getTitle();
                this.type = ClassNameUtils.STRING;
                this.hasPrincipalField = f.getPrincipalField().equals("true");
                this.hasPassword = f.getPassword().equals("true");
                this.hasUnique = f.getUnique().equals("true");
                this.nameColumn = f.getNamecolumn();
                this.hasNullable = f.getNullable().equals("true");
            } else if (field instanceof DomainModels.Entity.Query f) {
                this.name = f.getName();
                this.hasQuery = true;
            } else {
                throw new GeneratorException("cannot create field %s".formatted(field.getClass().getName()));
            }
        }

        public boolean hasType(Class<?> clazz) {
            return this.originClass.getName().equals(clazz.getName());
        }

        public String getName() {
            return name;
        }

        public String getTitle() {
            return title;
        }

        public String getMappedBy() {
            return mappedBy;
        }

        public TypeName getType() {
            return type;
        }

        public boolean isRelation() {
            return hasRelation;
        }

        public TypeName getOriginType() {
            return originType;
        }

        public boolean isPrincipalField() {
            return hasPrincipalField;
        }

        public boolean isQueryField() {
            return hasQuery;
        }

        public boolean isPassword() {
            return hasPassword;
        }

        public boolean isUnique() {
            return hasUnique;
        }

        public String getNameColumn() {
            return nameColumn;
        }

        public boolean isNullable() {
            return hasNullable;
        }

        public String getCurrentEntityName() {
            return currentEntityName;
        }
    }
}
