package com.taliro.quickdev.plugin.services;

import com.taliro.quickdev.plugin.fromxsd.DomainModels;
import com.taliro.quickdev.plugin.services.filegenerator.ClassFileGenerator;
import com.taliro.quickdev.plugin.services.filegenerator.EnumFileGenerator;
import com.taliro.quickdev.plugin.services.filegenerator.RepositoryFileGenerator;
import com.taliro.quickdev.plugin.services.filegenerator.SubProject;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class GeneratorService {

    public GeneratorService() {}

    public void generateFiles(Path path) throws IOException {
        List<SubProject> list = Arrays
            .stream(Objects.requireNonNull(path.resolve("modules").toFile().listFiles()))
            .map(SubProject::new)
            .toList();

        for (SubProject subProject : list) {
            try {
                //                boolean hasDeleted = false;
                for (File file : subProject.getDomain()) {
                    JAXBContext jaxbContext = JAXBContext.newInstance(DomainModels.class);
                    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
                    DomainModels domainModels = (DomainModels) jaxbUnmarshaller.unmarshal(file);

                    //                    Remove because file is now drop to build folder
                    //                    if (!hasDeleted) {
                    //                        File fileToDelete = subProject
                    //                            .getPath()
                    //                            .resolve("src/main/java")
                    //                            .resolve(String.join("/", domainModels.getEntity().getPackage().split("\\.")))
                    //                            .toFile();
                    //                        FileUtils.deleteDirectory(fileToDelete);
                    //                        hasDeleted = true;
                    //                    }

                    Fields fields = new Fields(
                        domainModels.getEntity().getStringOrBooleanOrDate(),
                        domainModels.getEntity().getName()
                    );
                    new ClassFileGenerator(domainModels, fields, subProject).generateFile();
                    new EnumFileGenerator(domainModels, subProject).generateFile();
                    new RepositoryFileGenerator(domainModels, fields, subProject).generateFile();
                }
            } catch (JAXBException | IOException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
        }
    }
}
