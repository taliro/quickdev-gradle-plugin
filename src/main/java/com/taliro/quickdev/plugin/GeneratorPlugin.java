package com.taliro.quickdev.plugin;

import com.taliro.quickdev.plugin.services.GeneratorService;
import org.gradle.api.Plugin;
import org.gradle.api.Project;

import java.io.IOException;

public class GeneratorPlugin implements Plugin<Project> {

    @Override
    public void apply(Project target) {
        try {
            new GeneratorService().generateFiles(target.getProjectDir().toPath());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
