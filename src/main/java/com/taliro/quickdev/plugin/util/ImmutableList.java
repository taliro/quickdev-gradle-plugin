package com.taliro.quickdev.plugin.util;

import java.util.Collection;
import java.util.List;
import java.util.Set;

public interface ImmutableList<E> extends Dictionary<E> {
    static <T> ImmutableList<T> of(Collection<T> collection) {
        return new Arrays<>(collection);
    }

    static <T> ImmutableList<T> of() {
        return new Arrays<>();
    }

    static <T> ImmutableList<T> of(T[] array) {
        return new Arrays<>(array);
    }

    /**
     * Create new array of range
     * @param start
     * @param end
     * @return
     */
    ImmutableList<E> range(int start, int end);

    /**
     * Create new array of range
     * @param end
     * @return
     */
    ImmutableList<E> rangeTo(int end);

    /**
     * Create new array of range
     * @param start
     * @return
     */
    ImmutableList<E> rangeFrom(int start);

    @SuppressWarnings("unchecked")
    default MutableList<E> toMutable() {
        return copy();
    }

    // Comparison and hashing

    /**
     * Compares the specified object with this collection for equality. <p>
     *
     * While the {@code Collection} interface adds no stipulations to the
     * general contract for the {@code Object.equals}, programmers who
     * implement the {@code Collection} interface "directly" (in other words,
     * create a class that is a {@code Collection} but is not a {@code Set}
     * or a {@code List}) must exercise care if they choose to override the
     * {@code Object.equals}.  It is not necessary to do so, and the simplest
     * course of action is to rely on {@code Object}'s implementation, but
     * the implementor may wish to implement a "value comparison" in place of
     * the default "reference comparison."  (The {@code List} and
     * {@code Set} interfaces mandate such value comparisons.)<p>
     *
     * The general contract for the {@code Object.equals} method states that
     * equals must be symmetric (in other words, {@code a.equals(b)} if and
     * only if {@code b.equals(a)}).  The contracts for {@code List.equals}
     * and {@code Set.equals} state that lists are only equal to other lists,
     * and sets to other sets.  Thus, a custom {@code equals} method for a
     * collection class that implements neither the {@code List} nor
     * {@code Set} interface must return {@code false} when this collection
     * is compared to any list or set.  (By the same logic, it is not possible
     * to write a class that correctly implements both the {@code Set} and
     * {@code List} interfaces.)
     *
     * @param o object to be compared for equality with this collection
     * @return {@code true} if the specified object is equal to this
     * collection
     *
     * @see Object#equals(Object)
     * @see Set#equals(Object)
     * @see List#equals(Object)
     */
    boolean equals(Object o);

    /**
     * Returns the hash code value for this collection.  While the
     * {@code Collection} interface adds no stipulations to the general
     * contract for the {@code Object.hashCode} method, programmers should
     * take note that any class that overrides the {@code Object.equals}
     * method must also override the {@code Object.hashCode} method in order
     * to satisfy the general contract for the {@code Object.hashCode} method.
     * In particular, {@code c1.equals(c2)} implies that
     * {@code c1.hashCode()==c2.hashCode()}.
     *
     * @return the hash code value for this collection
     *
     * @see Object#hashCode()
     * @see Object#equals(Object)
     */
    int hashCode();
}
