package com.taliro.quickdev.plugin.util;

import java.util.Collection;

public interface MutableList<E> extends Dictionary<E> {
    @SuppressWarnings("unchecked")
    default ImmutableList<E> toImmutable() {
        return copy();
    }

    static <T> MutableList<T> of(Collection<T> collection) {
        return new Arrays<>(collection);
    }

    static <T> MutableList<T> of(T[] array) {
        return new Arrays<>(array);
    }

    static <T> MutableList<T> of() {
        return new Arrays<>();
    }

    /**
     * Create new array of range
     * @param start
     * @param end
     * @return
     */
    MutableList<E> range(int start, int end);

    /**
     * Create new array of range
     * @param end
     * @return
     */
    MutableList<E> rangeTo(int end);

    /**
     * Create new array of range
     * @param start
     * @return
     */
    MutableList<E> rangeFrom(int start);
    /**
     * Return new String and joining by separator
     * @param separator
     * @return
     */

    /**
     * get last element and remove from array
     * @return
     */
    E pop();

    /**
     * get first element and remove from array
     * @return
     */
    E shift();
}
