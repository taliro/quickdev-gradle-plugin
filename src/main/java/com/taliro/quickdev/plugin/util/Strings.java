package com.taliro.quickdev.plugin.util;

public class Strings {

    private final String str;

    private Strings(String str) {
        this.str = str;
    }

    public static Strings of(String str) {
        return new Strings(str);
    }

    public MutableList<String> split(String regex) {
        return MutableList.of(str.split(regex));
    }

    public Strings join(String element) {
        return Strings.of(this.str + element);
    }

    public Strings join(Strings element) {
        return Strings.of(this.str + element.toString());
    }

    public Strings replaceAll(String regex, String replacement) {
        return Strings.of(str.replaceAll(regex, replacement));
    }

    public Strings trim() {
        return Strings.of(str.trim());
    }

    public Strings convertToCamelCase() {
        String string = toString();
        if (string == null || string.isEmpty()) {
            return Strings.of(string);
        }
        return Strings.of(Character.toLowerCase(string.charAt(0)) + string.substring(1));
    }

    public Strings convertCamelToSnake() {
        StringBuilder snakeCase = new StringBuilder();
        for (char character : str.toCharArray()) {
            if (Character.isUpperCase(character)) {
                snakeCase.append("_").append(Character.toLowerCase(character));
            } else {
                snakeCase.append(character);
            }
        }
        // Remove the leading underscore if present
        if (!snakeCase.isEmpty() && snakeCase.charAt(0) == '_') {
            snakeCase.deleteCharAt(0);
        }
        return Strings.of(snakeCase.toString());
    }

    @Override
    public String toString() {
        return str;
    }
}
