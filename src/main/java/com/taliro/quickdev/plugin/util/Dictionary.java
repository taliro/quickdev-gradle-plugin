package com.taliro.quickdev.plugin.util;

import java.util.Collection;
import java.util.Set;

public interface Dictionary<E> extends Collection<E> {
    /**
     * Return new String and joining by separator
     * @param separator
     * @return
     */
    String join(String separator);

    /**
     * Returns {@code true} if this collection contains no elements.
     *
     * @return {@code true} if this collection contains no elements
     */
    boolean isEmpty();

    int indexOf(E element);

    boolean exist(E e);

    E find(E e);

    Set<E> toSet();

    Arrays<E> copy();

    E last();

    E first();

    E get(int index);

    Strings toStrings();
}
