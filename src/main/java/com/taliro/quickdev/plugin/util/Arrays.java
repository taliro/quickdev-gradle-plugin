package com.taliro.quickdev.plugin.util;

import java.lang.reflect.Array;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class Arrays<T> implements Collection<T>, ImmutableList<T>, MutableList<T> {

    Object[] array;

    private static final Object[] EMPTY_ELEMENTDATA = {};

    public Arrays(T[] array) {
        this.array = array;
    }

    public Arrays() {
        this.array = EMPTY_ELEMENTDATA;
    }

    public Arrays(Collection<T> collection) {
        this.array = collection.toArray();
    }

    @SuppressWarnings("unchecked")
    public Arrays<T> range(int start, int end) {
        if (start < 0) {
            start = array.length - Math.abs(start);
        }

        if (end < 0) {
            end = array.length - Math.abs(end);
        }

        if (array.length == 0) {
            return this;
        }

        Object newArray = Array.newInstance(array[0].getClass(), (end + 1) - start);
        int j = 0;
        for (int i = start; i <= end && i < array.length; i++) {
            Array.set(newArray, j, array[i]);
            j++;
        }

        return new Arrays<>((T[]) newArray);
    }

    public Arrays<T> rangeTo(int end) {
        return range(0, end);
    }

    public Arrays<T> rangeFrom(int start) {
        return range(start, -1);
    }

    @Override
    public T shift() {
        T first = first();
        Arrays<T> ts = rangeFrom(1);
        array = ts.array;
        return first;
    }

    @Override
    public T pop() {
        T last = last();
        Arrays<T> ts = rangeTo(-2);
        array = ts.array;
        return last;
    }

    public String join(String separator) {
        return java.util.Arrays.stream(array).map(Object::toString).collect(Collectors.joining(separator));
    }

    @Override
    @SuppressWarnings("unchecked")
    public Set<T> toSet() {
        return (Set<T>) java.util.Arrays.stream(this.array).collect(Collectors.toSet());
    }

    @Override
    public T find(T t) {
        return stream().filter(it -> it.equals(t)).findFirst().orElse(null);
    }

    @Override
    public boolean exist(T t) {
        return stream().filter(it -> it.equals(t)).anyMatch(it -> it.equals(t));
    }

    @SuppressWarnings("unchecked")
    public T last() {
        if (array.length == 0) return null;
        return (T) array[array.length - 1];
    }

    @SuppressWarnings("unchecked")
    public T first() {
        if (array.length == 0) return null;
        return (T) array[0];
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(int index) {
        if (index > array.length - 1) {
            throw new ArrayIndexOutOfBoundsException();
        }
        return (T) array[index];
    }

    @Override
    public Strings toStrings() {
        return Strings.of(join(""));
    }

    public int indexOf(T element) {
        int index = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(element)) {
                break;
            }
            index++;
        }
        return index;
    }

    @Override
    public int size() {
        return array.length;
    }

    @Override
    public boolean isEmpty() {
        return array.length == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(o)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<T> iterator() {
        return new Iter();
    }

    @Override
    public Object[] toArray() {
        return array;
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T1> T1[] toArray(T1[] a) {
        return (T1[]) array;
    }

    @Override
    public <T1> T1[] toArray(IntFunction<T1[]> generator) {
        return java.util.Arrays.stream(array).toArray(generator);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Arrays<T> copy() {
        return (Arrays<T>) new Arrays<>(array);
    }

    @Override
    @SuppressWarnings("unchecked")
    public boolean add(T t) {
        try {
            Object[] tmp = java.util.Arrays.copyOf(array, array.length + 1);
            tmp[array.length] = t;
            array = tmp;
            return true;
        } catch (NegativeArraySizeException ignored) {
            return false;
        }
    }

    @Override
    public boolean remove(Object o) {
        try {
            Iter iter = new Iter();
            while (iter.hasNext()) {
                iter.remove();
            }
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            for (Object o : c) {
                if (array[i].equals(o)) {
                    count++;
                }
            }
        }
        return count == c.size();
    }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        try {
            array = c.toArray();
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        try {
            Iter iter = new Iter();
            while (iter.hasNext()) {
                T next = iter.next();
                for (Object o : c) {
                    if (next.equals(o)) {
                        iter.remove();
                    }
                }
            }
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    @Override
    public boolean removeIf(Predicate<? super T> filter) {
        try {
            Iter iter = new Iter();
            while (iter.hasNext()) {
                T next = iter.next();
                if (filter.test(next)) {
                    iter.remove();
                }
            }
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        try {
            Iter iter = new Iter();
            while (iter.hasNext()) {
                T next = iter.next();
                for (Object o : c) {
                    if (!next.equals(o)) {
                        iter.remove();
                    }
                }
            }
            return true;
        } catch (Exception ignored) {
            return false;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public void clear() {
        array = (T[]) Array.newInstance(array[0].getClass(), 0);
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    private class Iter implements Iterator<T> {

        int cursor = 0;

        private Iter() {}

        @Override
        public boolean hasNext() {
            return cursor != array.length;
        }

        @Override
        @SuppressWarnings("unchecked")
        public T next() {
            T t = (T) array[cursor];
            cursor++;
            return t;
        }

        @Override
        @SuppressWarnings("unchecked")
        public void remove() {
            T[] newArray = (T[]) Array.newInstance(array[0].getClass(), array.length - 2);

            for (int i = 0; i < array.length; i++) {
                if (i == cursor) continue;
                newArray[i] = (T) array[i];
            }

            array = newArray;
        }

        @Override
        @SuppressWarnings("unchecked")
        public void forEachRemaining(Consumer<? super T> action) {
            for (int i = 0; i < array.length; i++) {
                action.accept((T) array[i]);
            }
        }
    }
}
