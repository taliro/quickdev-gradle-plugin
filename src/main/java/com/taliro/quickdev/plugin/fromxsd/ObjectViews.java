//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2024.01.28 à 11:55:37 AM CET 
//


package com.taliro.quickdev.plugin.fromxsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import org.w3c.dom.Element;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="router"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;attribute name="path"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                       &lt;pattern value="/[a-zA-Z]*"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *                 &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *                 &lt;attribute name="authentication"&gt;
 *                   &lt;simpleType&gt;
 *                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *                       &lt;enumeration value="true"/&gt;
 *                       &lt;enumeration value="false"/&gt;
 *                     &lt;/restriction&gt;
 *                   &lt;/simpleType&gt;
 *                 &lt;/attribute&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="view"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;choice maxOccurs="unbounded"&gt;
 *                   &lt;element name="toolbar" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *                   &lt;element name="menu"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;choice&gt;
 *                             &lt;element name="leftBarMenu" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *                             &lt;element name="topBarMenu" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
 *                           &lt;/choice&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="content"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;choice&gt;
 *                             &lt;any processContents='lax' maxOccurs="unbounded" minOccurs="0"/&gt;
 *                           &lt;/choice&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/choice&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "router",
    "view"
})
@XmlRootElement(name = "object-views", namespace = "http://quickdev.com/object-views_1.0.0")
public class ObjectViews {

    @XmlElement(namespace = "http://quickdev.com/object-views_1.0.0", required = true)
    protected ObjectViews.Router router;
    @XmlElement(namespace = "http://quickdev.com/object-views_1.0.0", required = true)
    protected ObjectViews.View view;

    /**
     * Obtient la valeur de la propriété router.
     * 
     * @return
     *     possible object is
     *     {@link ObjectViews.Router }
     *     
     */
    public ObjectViews.Router getRouter() {
        return router;
    }

    /**
     * Définit la valeur de la propriété router.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectViews.Router }
     *     
     */
    public void setRouter(ObjectViews.Router value) {
        this.router = value;
    }

    /**
     * Obtient la valeur de la propriété view.
     * 
     * @return
     *     possible object is
     *     {@link ObjectViews.View }
     *     
     */
    public ObjectViews.View getView() {
        return view;
    }

    /**
     * Définit la valeur de la propriété view.
     * 
     * @param value
     *     allowed object is
     *     {@link ObjectViews.View }
     *     
     */
    public void setView(ObjectViews.View value) {
        this.view = value;
    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;attribute name="path"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *             &lt;pattern value="/[a-zA-Z]*"/&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *       &lt;attribute name="name" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
     *       &lt;attribute name="authentication"&gt;
     *         &lt;simpleType&gt;
     *           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
     *             &lt;enumeration value="true"/&gt;
     *             &lt;enumeration value="false"/&gt;
     *           &lt;/restriction&gt;
     *         &lt;/simpleType&gt;
     *       &lt;/attribute&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Router {

        @XmlAttribute(name = "path")
        protected String path;
        @XmlAttribute(name = "name")
        protected String name;
        @XmlAttribute(name = "authentication")
        protected String authentication;

        /**
         * Obtient la valeur de la propriété path.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPath() {
            return path;
        }

        /**
         * Définit la valeur de la propriété path.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPath(String value) {
            this.path = value;
        }

        /**
         * Obtient la valeur de la propriété name.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getName() {
            return name;
        }

        /**
         * Définit la valeur de la propriété name.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setName(String value) {
            this.name = value;
        }

        /**
         * Obtient la valeur de la propriété authentication.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAuthentication() {
            return authentication;
        }

        /**
         * Définit la valeur de la propriété authentication.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAuthentication(String value) {
            this.authentication = value;
        }

    }


    /**
     * <p>Classe Java pour anonymous complex type.
     * 
     * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;choice maxOccurs="unbounded"&gt;
     *         &lt;element name="toolbar" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
     *         &lt;element name="menu"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;choice&gt;
     *                   &lt;element name="leftBarMenu" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
     *                   &lt;element name="topBarMenu" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
     *                 &lt;/choice&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="content"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;choice&gt;
     *                   &lt;any processContents='lax' maxOccurs="unbounded" minOccurs="0"/&gt;
     *                 &lt;/choice&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/choice&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "toolbarOrMenuOrContent"
    })
    public static class View {

        @XmlElements({
            @XmlElement(name = "toolbar", namespace = "http://quickdev.com/object-views_1.0.0"),
            @XmlElement(name = "menu", namespace = "http://quickdev.com/object-views_1.0.0", type = ObjectViews.View.Menu.class),
            @XmlElement(name = "content", namespace = "http://quickdev.com/object-views_1.0.0", type = ObjectViews.View.Content.class)
        })
        protected List<Object> toolbarOrMenuOrContent;

        /**
         * Gets the value of the toolbarOrMenuOrContent property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the toolbarOrMenuOrContent property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getToolbarOrMenuOrContent().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Object }
         * {@link ObjectViews.View.Menu }
         * {@link ObjectViews.View.Content }
         * 
         * 
         */
        public List<Object> getToolbarOrMenuOrContent() {
            if (toolbarOrMenuOrContent == null) {
                toolbarOrMenuOrContent = new ArrayList<Object>();
            }
            return this.toolbarOrMenuOrContent;
        }


        /**
         * <p>Classe Java pour anonymous complex type.
         * 
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;choice&gt;
         *         &lt;any processContents='lax' maxOccurs="unbounded" minOccurs="0"/&gt;
         *       &lt;/choice&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "any"
        })
        public static class Content {

            @XmlAnyElement(lax = true)
            protected List<Object> any;

            /**
             * Gets the value of the any property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the any property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getAny().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Element }
             * {@link Object }
             * 
             * 
             */
            public List<Object> getAny() {
                if (any == null) {
                    any = new ArrayList<Object>();
                }
                return this.any;
            }

        }


        /**
         * <p>Classe Java pour anonymous complex type.
         * 
         * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;choice&gt;
         *         &lt;element name="leftBarMenu" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
         *         &lt;element name="topBarMenu" type="{http://www.w3.org/2001/XMLSchema}anyType"/&gt;
         *       &lt;/choice&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "leftBarMenu",
            "topBarMenu"
        })
        public static class Menu {

            @XmlElement(namespace = "http://quickdev.com/object-views_1.0.0")
            protected Object leftBarMenu;
            @XmlElement(namespace = "http://quickdev.com/object-views_1.0.0")
            protected Object topBarMenu;

            /**
             * Obtient la valeur de la propriété leftBarMenu.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getLeftBarMenu() {
                return leftBarMenu;
            }

            /**
             * Définit la valeur de la propriété leftBarMenu.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setLeftBarMenu(Object value) {
                this.leftBarMenu = value;
            }

            /**
             * Obtient la valeur de la propriété topBarMenu.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getTopBarMenu() {
                return topBarMenu;
            }

            /**
             * Définit la valeur de la propriété topBarMenu.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setTopBarMenu(Object value) {
                this.topBarMenu = value;
            }

        }

    }

}
