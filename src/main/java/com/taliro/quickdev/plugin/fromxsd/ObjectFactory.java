//
// Ce fichier a été généré par l'implémentation de référence JavaTM Architecture for XML Binding (JAXB), v2.3.2 
// Voir <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Toute modification apportée à ce fichier sera perdue lors de la recompilation du schéma source. 
// Généré le : 2024.01.28 à 11:55:37 AM CET 
//


package com.taliro.quickdev.plugin.fromxsd;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.taliro.quickdev.plugin.fromxsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.taliro.quickdev.plugin.fromxsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DomainModels }
     * 
     */
    public DomainModels createDomainModels() {
        return new DomainModels();
    }

    /**
     * Create an instance of {@link ObjectViews }
     * 
     */
    public ObjectViews createObjectViews() {
        return new ObjectViews();
    }

    /**
     * Create an instance of {@link ObjectViews.View }
     * 
     */
    public ObjectViews.View createObjectViewsView() {
        return new ObjectViews.View();
    }

    /**
     * Create an instance of {@link DomainModels.Enum }
     * 
     */
    public DomainModels.Enum createDomainModelsEnum() {
        return new DomainModels.Enum();
    }

    /**
     * Create an instance of {@link DomainModels.Entity }
     * 
     */
    public DomainModels.Entity createDomainModelsEntity() {
        return new DomainModels.Entity();
    }

    /**
     * Create an instance of {@link ObjectViews.Router }
     * 
     */
    public ObjectViews.Router createObjectViewsRouter() {
        return new ObjectViews.Router();
    }

    /**
     * Create an instance of {@link ObjectViews.View.Menu }
     * 
     */
    public ObjectViews.View.Menu createObjectViewsViewMenu() {
        return new ObjectViews.View.Menu();
    }

    /**
     * Create an instance of {@link ObjectViews.View.Content }
     * 
     */
    public ObjectViews.View.Content createObjectViewsViewContent() {
        return new ObjectViews.View.Content();
    }

    /**
     * Create an instance of {@link DomainModels.Enum.Item }
     * 
     */
    public DomainModels.Enum.Item createDomainModelsEnumItem() {
        return new DomainModels.Enum.Item();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.String }
     * 
     */
    public DomainModels.Entity.String createDomainModelsEntityString() {
        return new DomainModels.Entity.String();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.Boolean }
     * 
     */
    public DomainModels.Entity.Boolean createDomainModelsEntityBoolean() {
        return new DomainModels.Entity.Boolean();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.Date }
     * 
     */
    public DomainModels.Entity.Date createDomainModelsEntityDate() {
        return new DomainModels.Entity.Date();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.Datetime }
     * 
     */
    public DomainModels.Entity.Datetime createDomainModelsEntityDatetime() {
        return new DomainModels.Entity.Datetime();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.Long }
     * 
     */
    public DomainModels.Entity.Long createDomainModelsEntityLong() {
        return new DomainModels.Entity.Long();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.Integer }
     * 
     */
    public DomainModels.Entity.Integer createDomainModelsEntityInteger() {
        return new DomainModels.Entity.Integer();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.Decimal }
     * 
     */
    public DomainModels.Entity.Decimal createDomainModelsEntityDecimal() {
        return new DomainModels.Entity.Decimal();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.Binary }
     * 
     */
    public DomainModels.Entity.Binary createDomainModelsEntityBinary() {
        return new DomainModels.Entity.Binary();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.Enum }
     * 
     */
    public DomainModels.Entity.Enum createDomainModelsEntityEnum() {
        return new DomainModels.Entity.Enum();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.OneToMany }
     * 
     */
    public DomainModels.Entity.OneToMany createDomainModelsEntityOneToMany() {
        return new DomainModels.Entity.OneToMany();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.ManyToMany }
     * 
     */
    public DomainModels.Entity.ManyToMany createDomainModelsEntityManyToMany() {
        return new DomainModels.Entity.ManyToMany();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.ManyToOne }
     * 
     */
    public DomainModels.Entity.ManyToOne createDomainModelsEntityManyToOne() {
        return new DomainModels.Entity.ManyToOne();
    }

    /**
     * Create an instance of {@link DomainModels.Entity.Query }
     * 
     */
    public DomainModels.Entity.Query createDomainModelsEntityQuery() {
        return new DomainModels.Entity.Query();
    }

}
